##Extreure informació de la memoria
```
lsblk -o (columna)
lsblk -S


[root@a12 ~]# lsblk -o NAME,MODEL
çNAME   MODEL
sda    ST500DM002-1BD142
├─sda1 
├─sda5 
├─sda6 
└─sda7 

[root@a12 ~]# lsblk -o NAME,SIZE,MOUNTPOINT,MODEL
NAME     SIZE MOUNTPOINT MODEL
sda    465.8G            ST500DM002-1BD142
├─sda1     1K            
├─sda5   100G /          
├─sda6   100G            
└─sda7     5G [SWAP]    

[root@a12 ~]# lsblk -S
NAME HCTL       TYPE VENDOR   MODEL              REV TRAN
sda  0:0:0:0    disk ATA      ST500DM002-1BD142 KC48 sata

##Alias 

Alias per crear una comanda que mostri les columnes interessants d'un disk que m'ofereix la comanda lsblk:

[root@a12 ~]# alias lsdisk='lsblk -o NAME,SIZE,MOUNTPOINT,MODEL'
[root@a12 ~]# lsdisk
NAME     SIZE MOUNTPOINT MODEL
sda    465.8G            ST500DM002-1BD142
├─sda1     1K            
├─sda5   100G /          
├─sda6   100G            
└─sda7     5G [SWAP]   

##SMART

Serveix per veure quant el disc dur donarà problemes
smartctl -a /dev/sda

[root@a12 ~]# smartctl -a /dev/sda
smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.15-201.fc32.x86_64] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Seagate Barracuda 7200.14 (AF)
Device Model:     ST500DM002-1BD142
Serial Number:    Z6E4HGEJ
LU WWN Device Id: 5 000c50 066e0bd56
Firmware Version: KC48
User Capacity:    500,107,862,016 bytes [500 GB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    7200 rpm
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS T13/1699-D revision 4
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Tue Nov 10 09:35:23 2020 CET
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x82)	Offline data collection activity
					was completed without error.
					Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(  609) seconds.
Offline data collection
capabilities: 			 (0x7b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (  87) minutes.
Conveyance self-test routine
recommended polling time: 	 (   2) minutes.
SCT capabilities: 	       (0x303f)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x000f   107   099   006    Pre-fail  Always       -       13781552
  3 Spin_Up_Time            0x0003   100   099   000    Pre-fail  Always       -       0
  4 Start_Stop_Count        0x0032   097   097   020    Old_age   Always       -       3170
  5 Reallocated_Sector_Ct   0x0033   100   100   036    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000f   078   060   030    Pre-fail  Always       -       8714517386
  9 Power_On_Hours          0x0032   091   091   000    Old_age   Always       -       7910
 10 Spin_Retry_Count        0x0013   100   100   097    Pre-fail  Always       -       0
 12 Power_Cycle_Count       0x0032   097   097   020    Old_age   Always       -       3166
183 Runtime_Bad_Block       0x0032   100   100   000    Old_age   Always       -       0
184 End-to-End_Error        0x0032   100   100   099    Old_age   Always       -       0
187 Reported_Uncorrect      0x0032   014   014   000    Old_age   Always       -       86
188 Command_Timeout         0x0032   100   099   000    Old_age   Always       -       0 0 2
189 High_Fly_Writes         0x003a   100   100   000    Old_age   Always       -       0
190 Airflow_Temperature_Cel 0x0022   068   053   045    Old_age   Always       -       32 (Min/Max 19/32)
194 Temperature_Celsius     0x0022   032   047   000    Old_age   Always       -       32 (0 11 0 0 0)
195 Hardware_ECC_Recovered  0x001a   053   033   000    Old_age   Always       -       13781552
197 Current_Pending_Sector  0x0012   100   100   000    Old_age   Always       -       8
198 Offline_Uncorrectable   0x0010   100   100   000    Old_age   Offline      -       8
199 UDMA_CRC_Error_Count    0x003e   200   200   000    Old_age   Always       -       0
240 Head_Flying_Hours       0x0000   100   253   000    Old_age   Offline      -       7950h+02m+55.164s
241 Total_LBAs_Written      0x0000   100   253   000    Old_age   Offline      -       1514750543
242 Total_LBAs_Read         0x0000   100   253   000    Old_age   Offline      -       175571956

SMART Error Log Version: 1
ATA Error Count: 86 (device log contains only the most recent five errors)
	CR = Command Register [HEX]
	FR = Features Register [HEX]
	SC = Sector Count Register [HEX]
	SN = Sector Number Register [HEX]
	CL = Cylinder Low Register [HEX]
	CH = Cylinder High Register [HEX]
	DH = Device/Head Register [HEX]
	DC = Device Command Register [HEX]
	ER = Error register [HEX]
	ST = Status register [HEX]
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 86 occurred at disk power-on lifetime: 6621 hours (275 days + 21 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 ff ff ff 0f  Error: UNC at LBA = 0x0fffffff = 268435455

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  60 00 08 ff ff ff 4f 00      00:57:25.506  READ FPDMA QUEUED
  61 00 08 ff ff ff 4f 00      00:57:25.506  WRITE FPDMA QUEUED
  ea 00 00 00 00 00 a0 00      00:57:25.506  FLUSH CACHE EXT
  27 00 00 00 00 00 e0 00      00:57:25.505  READ NATIVE MAX ADDRESS EXT [OBS-ACS-3]
  ec 00 00 00 00 00 a0 00      00:57:25.504  IDENTIFY DEVICE

Error 85 occurred at disk power-on lifetime: 6621 hours (275 days + 21 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 ff ff ff 0f  Error: UNC at LBA = 0x0fffffff = 268435455

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  60 00 01 ff ff ff 4f 00      00:57:21.784  READ FPDMA QUEUED
  27 00 00 00 00 00 e0 00      00:57:21.784  READ NATIVE MAX ADDRESS EXT [OBS-ACS-3]
  ec 00 00 00 00 00 a0 00      00:57:21.782  IDENTIFY DEVICE
  ef 03 46 00 00 00 a0 00      00:57:21.782  SET FEATURES [Set transfer mode]
  27 00 00 00 00 00 e0 00      00:57:21.782  READ NATIVE MAX ADDRESS EXT [OBS-ACS-3]

Error 84 occurred at disk power-on lifetime: 6621 hours (275 days + 21 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 ff ff ff 0f  Error: UNC at LBA = 0x0fffffff = 268435455

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  60 00 01 ff ff ff 4f 00      00:57:17.922  READ FPDMA QUEUED
  60 00 01 ff ff ff 4f 00      00:57:17.921  READ FPDMA QUEUED
  27 00 00 00 00 00 e0 00      00:57:17.921  READ NATIVE MAX ADDRESS EXT [OBS-ACS-3]
  ec 00 00 00 00 00 a0 00      00:57:17.919  IDENTIFY DEVICE
  ef 03 46 00 00 00 a0 00      00:57:17.919  SET FEATURES [Set transfer mode]

Error 83 occurred at disk power-on lifetime: 6621 hours (275 days + 21 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 ff ff ff 0f  Error: UNC at LBA = 0x0fffffff = 268435455

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  60 00 01 ff ff ff 4f 00      00:57:14.076  READ FPDMA QUEUED
  60 00 01 ff ff ff 4f 00      00:57:14.075  READ FPDMA QUEUED
  60 00 01 ff ff ff 4f 00      00:57:14.075  READ FPDMA QUEUED
  27 00 00 00 00 00 e0 00      00:57:14.074  READ NATIVE MAX ADDRESS EXT [OBS-ACS-3]
  ec 00 00 00 00 00 a0 00      00:57:14.073  IDENTIFY DEVICE

Error 82 occurred at disk power-on lifetime: 6621 hours (275 days + 21 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 ff ff ff 0f  Error: UNC at LBA = 0x0fffffff = 268435455

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  60 00 01 ff ff ff 4f 00      00:57:10.229  READ FPDMA QUEUED
  60 00 01 ff ff ff 4f 00      00:57:10.229  READ FPDMA QUEUED
  60 00 01 ff ff ff 4f 00      00:57:10.229  READ FPDMA QUEUED
  60 00 01 ff ff ff 4f 00      00:57:10.228  READ FPDMA QUEUED
  27 00 00 00 00 00 e0 00      00:57:10.228  READ NATIVE MAX ADDRESS EXT [OBS-ACS-3]

SMART Self-test log structure revision number 1
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

Performance

Secuencial: Cuando solo estoy haciendo una lectura


df -h

[root@a12 ~]# df -h
Filesystem                                                           Size  Used Avail Use% Mounted on
devtmpfs                                                             3.9G     0  3.9G   0% /dev
tmpfs                                                                3.9G   50M  3.8G   2% /dev/shm
tmpfs                                                                3.9G  1.7M  3.9G   1% /run
/dev/sda5                                                             98G   52G   42G  56% /
tmpfs                                                                3.9G  124K  3.9G   1% /tmp
tmpfs                                                                785M  104K  785M   1% /run/user/102364
gandhi.informatica.escoladeltreball.org:/users/inf/hism1/ism5677930  931G  255G  677G  28% /home/users/inf/hism1/ism5677930/ism5677930

dd if

[root@a12 ~]# dd if=/dev/urandom of=/mnt/prova2G bs=1024 count=2048
2048+0 records in
2048+0 records out
2097152 bytes (2.1 MB, 2.0 MiB) copied, 0.0306602 s, 68.4 MB/s

[root@a12 ~]# dd if=/dev/urandom of=/mnt/prova2G bs=1024 count=2048
2048+0 records in
2048+0 records out
2097152 bytes (2.1 MB, 2.0 MiB) copied, 0.0306602 s, 68.4 MB/s

rm -f date; cp /mnt/prova2G /root/prova2G; date
```
##11 Nov
```
cd add .
cd hwlovelinux
git commit -am
git pull
git push

##Extreure informació amb lshw
lshw dona molta informació, per filtrar aquesta hem de cercar la comanda amb --help o man
lshw --businfo
lshw --short 
lshw -class disk
lshw -short -c disk 
dmidecode | less




##22Nov 
dmidecode -t baseboard-product-name
dmidecode -s baseboard-product-name
dmidecode -s baseboard-manufacturer
dmidecode -t baseboard
lscpu
lspci -nn

lshw -class memory
dmidecode -s system-product-name
dmidecode -s system-product-name
dmidecode -s mother-board
dmidecode -s baseboard-product-name

dmidecode -t system --version
dmidecode -s baseboard-product-name
dmidecode --help
dmidecode -t --help
dmidecode -s --help

dmidecode -t baseboard
dmidecode --type memory 
dmidecode -t memory


tmux a
dmidecode -t
dmidecode -s
dmidecode -t memory
dmidecode -t 16

lspci -nn |grep 0280
lspci -nn |grep 0200


lspci -nn -v -mm 
lspci -nn -v -mm |grep Ethernet 
lspci -nn -v -mm |grep "Ethernet controller"
lspci -nn -v -mm |grep -B 1 "Ethernet controller"
lspci -nn -v -mm |grep -B 1 -A 2 "Ethernet controller"
lspci -nn -v -mm |grep -B 1 -A 8 "Ethernet controller"

lspci -nn -v -mm -k 
ip a
ifconfig
ifconfig |less

lsblk -O |less

lsblk -o MODEL,NAME,SIZE,MOUNTPOINT

smartctl --help

smartctl --all /dev/sdb |less

lshw -class disk -sanitize

dmidecode -s baseboard-product-name
```
##Particiones 
```
Parted
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1% 50%
parted -s /dev/vda mkpart primary 50% 60%
parted -s /dev/vda mkpart primary 60% 70%
parted -s /dev/vda mkpart extended 70% 100%
parted -s /dev/vda mkpart logical 70% 75%
````